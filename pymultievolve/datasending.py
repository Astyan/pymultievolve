#!/usr/bin/env python
# encoding: utf-8

from multiprocessing import Queue, Manager
from Queue import Full

from optparse import OptionParser
import inspect

class GenericSending(object):
    """ a generic way to send data """
    
    def __init__(self):
        """ simple init"""
        pass
    
    def send_to(self, data, to):
        """ send data to an object
        @param data: the data to send
        @type data: any
        @param to: a GenericSending child 
        @type to: GenericSending
        """
        pass
    
    def get_data(self):
        """ get the receive data"""
        pass

class QueueSending(GenericSending):
    """send data through a Queue 
    @ivar q: the Queue
    @type q: Queue"""
    
    
    def __init__(self):
        """ simple init"""
        super(QueueSending ,self).__init__()
        self.q = Queue()
    
    def put(self, *args, **kwargs):
        """ put stuff in the queue"""
        self.q.put(*args, **kwargs)
    
    def get(self):
        """ get the last data in the queue"""
        return self.q.get()
    
    def empty(self):
        """ inform if the queue is empty"""
        return self.q.empty()
    
    def send_to(self, data, to):
         """ send data to a QueueSending
        @param data: the data to send
        @type data: any
        @param to: a QueueSending child 
        @type to: QueueSending"""
        try:
            to.put(data)
        except Full:
            return False
        return True
    
    def get_data(self):
        """ get all the data in the queue
        @return: if there is still data in the queue and the data
        @rtype: (bool, any)
        """
        #get the data an if there is still data in the queue
        if self.empty():
            return False, None
        data = self.get()
        if self.empty():
            return False, data
        else:
            return True, data


if __name__ == "__main__":
    #~ print inspect.getmembers(Queue, predicate=inspect.ismethod)
    qs = QueueSending()
    #~ print qs.__dict__
    #~ q = Queue()
    #~ print q.__dict__
    qs.put('lol')
    #~ qs.put('lol')
    #~ qs.put('lol')
    print qs.get()
    #~ print qs.get()
    #~ print qs.get()
    
