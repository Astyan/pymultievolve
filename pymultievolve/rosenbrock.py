import random, math


from algorithm import IsletManager
from crossover import CXTwoPoints
from mutation import MutUniform
from reduction import SelTournamentNoRemise, SelTournament
from evaluation import GenericEvaluation
from individual import PopulationQuickSeparation
from spacesearch import DEConstraints

#Rosenbrock is unimodal

class PopRosenbrock(PopulationQuickSeparation):
    
    def dna_generation(self):
        dna = []
        for i in range(0, self.list_len):
            dna.append(random.uniform(-2., 3.))
        return dna

    def vector_computation(self, dna):
        vector = [0]*self.list_len
        return vector
    

class EvalRosenbrock(GenericEvaluation):
    
    def evaluate(self, individual_dna):
        """ do the score on the rosenbrock function""" 
        line = math.sqrt(len(individual_dna))
        score = 0.
        for i in range(0, len(individual_dna)-1):
            score += self.value_function(individual_dna[i], individual_dna[i+1])
        return score

    def value_function(self, x, y):
        return (1.-x)**2.+100*(y - x**2)**2
        

if __name__ == "__main__":
    population = PopRosenbrock(list_len=11) # 10*10
    evaluation = EvalRosenbrock()
    reduction = SelTournament(goal="minimize", tournsize=7)
    tournament_de = SelTournament(goal="minimize", tournsize=7)
    deconstraints = DEConstraints([-2.]*11, [3.]*11)
    mutation = MutUniform(indpb=0.1, low=-2., up=3.)
    
    algo = IsletManager(mutation=mutation, reduction=reduction, 
                        evaluation=evaluation, population=population,
                        population_size=400, nb_generation=20, 
                        mutation_probability=0.1, 
                        do_stats=True, nb_islet=4, communication="QueueSending()",
                        tournament_de=tournament_de, nbselected_de=40,
                        differential_evolution_constraintes=deconstraints,
                        max_pop=1000, min_pop=100)
    algo.run()
