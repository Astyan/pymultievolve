#!/usr/bin/env python
# encoding: utf-8
import random


from algorithm import IsletManager
from crossover import CXTwoPoints
from mutation import MutUniformInt
from reduction import SelTournamentNoRemise, SelTournament
from evaluation import GenericEvaluation
from individual import PopulationQuickSeparation
from spacesearch import DEConstraints



class PopOneMax(PopulationQuickSeparation):
    
    def dna_generation(self):
        dna = []
        for i in range(0, self.list_len):
            dna.append(random.randint(0,1))
        return dna
    
    def vector_computation(self, dna):
        vector = [0]*self.list_len
        return vector
    
        
class EvalOneMax(GenericEvaluation):
    
    def evaluate(self, individual_dna):
        score = 0
        for number in individual_dna:
            score += number
        return score


if __name__ == "__main__":
    population = PopOneMax(list_len=50)
    evaluation = EvalOneMax()
    #~ reduction = SelTournamentNoRemise(goal="minimize", tournsize=7)
    reduction = SelTournamentNoRemise(goal="maximize", tournsize=7)
    tournament_de = SelTournament(goal="maximize", tournsize=7)
    crossover = None
    mutation = MutUniformInt(indpb=0.1, low=0, up=1)
    deconstraints = DEConstraints([0]*50, [1]*50)
    
    algo = IsletManager(mutation=mutation, reduction=reduction, 
                        evaluation=evaluation, population=population,
                        population_size=1000, nb_generation=40, 
                        mutation_probability=0.1, 
                        do_stats=True, nb_islet=4, communication="QueueSending()",
                        tournament_de=tournament_de, nbselected_de=40,
                        differential_evolution_constraintes=deconstraints,
                        max_pop=3000, min_pop=500)
    algo.run()
    
    
    
