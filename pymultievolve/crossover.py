#!/usr/bin/env python
# encoding: utf-8
import random
import copy

from individual import Individual

class CX(object):
    """ A generic crossover method, every crossover method must be 
    child of it"""
    
    def __init__(self, **kwargs):
        """ Generic initialisation """
        for key, value in kwargs.items():
            setattr(self, key, value)
    
    def crossover(self, individual1, individual2):
        """ action to do when the crossover append 
        @param individual1: an Individual
        @type individual1: Individual
        @param individual2: an Individual
        @type individual2: Individual
        @return: a couple of Individual with the dna generated from
        the 2 individual in entry
        @rtype: (Individual, Individual)"""
        raise NotImplementedError
    
    def vector_add(self, ind1, ind2, individual1, individual2):
        """ add the vector of the 2 individual created from the crossover
        the vector is generated with the individual use for the crossover
        @param ind1: a child individual 
        @type ind1: Individual
        @param ind2: a child individual
        @type ind2: Individual
        @param individual1: a parent individual 
        @type individual1: Individual
        @param individual2: a parent individual 
        @type individual2: Individual
        @return: the 2 new individual with there vector
        @rtype: (Individual, Individual)
        """
        if ind1.vector != None and ind2.vector != None:
            ind1.vector = individual1.vector + individual2.vector
            ind2.vector = individual1.vector + individual2.vector
        return ind1, ind2
    
    def run(self, individual1, individual2):
        """ do the crossover and add the vector
        @param individual1: a individual 
        @type individual1: Individual
        @param individual2: a individual 
        @type individual2: Individual
        @return: the 2 new individual from crossover with there vector
        @rtype: (Individual, Individual)
        """
        ind1, ind2 = self.crossover(copy.deepcopy(individual1), copy.deepcopy(individual2))
        #do the vector addition if vector != None
        ind1, ind2 = self.vector_add(ind1, ind2, individual1, individual2)
        ind1.score = None
        ind2.score = None
        return (ind1, ind2)
        
class CXOnePoint(CX):
    """ Simple OnePoint CrossOver """
    
    def crossover(self, individual1, individual2):
        """ A one point crossover
        @param individual1: an Individual child
        @type individual1: Individual
        @param individual2: an Individual child
        @type individual2: Individual
        @return: a couple of Individual with the dna generated from
        the 2 individual in entry
        @rtype: (Individual, Individual)"""
        if len(individual1.dna) != len(individual2.dna):
            raise Exception("To change")
        val = random.randint(0, len(individual1.dna))
        #cutting the dna
        i1_1_dna = individual1.dna[0: val]
        i1_2_dna = individual1.dna[val: len(individual1.dna)]
        i2_1_dna = individual2.dna[0: val]
        i2_2_dna = individual2.dna[val: len(individual2.dna)]
        i1_newdna = i1_1_dna + i2_2_dna
        i2_newdna = i2_1_dna + i1_2_dna
        individual1.dna = i1_newdna
        individual2.dna = i2_newdna
        return (individual1, individual2)

class CXTwoPoints(CX):
    """ Simple TwoPoint CrossOver """
    
    def crossover(self, individual1, individual2):
        """ a twopoint crossover 
        @param individual1: an Individual child
        @type individual1: Individual
        @param individual2: an Individual child
        @type individual2: Individual
        @return: a couple of Individual with the dna generated from
        the 2 individual in entry
        @rtype: (Individual, Individual)"""
        if len(individual1.dna) != len(individual2.dna):
            raise Exception("To change")
        val_1 = random.randint(0, len(individual1.dna))
        val_2 = random.randint(0, len(individual1.dna))
        mini = min(val_1, val_2)
        maxi = max(val_1, val_2)
        #cutting the dna
        i1_1_dna = individual1.dna[0 : mini]
        i1_2_dna = individual1.dna[mini : maxi]
        i1_3_dna = individual1.dna[maxi : len(individual1.dna)]
        i2_1_dna = individual2.dna[0 : mini]
        i2_2_dna = individual2.dna[mini : maxi]
        i2_3_dna = individual2.dna[maxi : len(individual1.dna)]
        i1_newdna = i1_1_dna + i2_2_dna + i1_3_dna
        i2_newdna = i2_1_dna + i1_2_dna + i2_3_dna
        individual1.dna = i1_newdna
        individual2.dna = i2_newdna
        return (individual1, individual2)

if __name__ == "__main__":
    i1 = Individual([32, 21])
    i2 = Individual([21, 32])
    cx = CXOnePoint()
    i1, i2 = cx.run(i1, i2)
    print i2.dna, i1.dna 
    
    
    
