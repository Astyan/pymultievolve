#!/usr/bin/env python
# encoding: utf-8
import random, signal
from copy import deepcopy

from multiprocesspool.multiprocesspool import MultiProcessPool
from multiprocessing import Process
from multiprocessing.queues import Queue
from multiprocessing import Manager

from datasending import QueueSending
from spacesearch import QuickSeparationAttr

class GenericGeneticAlgorithm(object):
    """GenericGeneticAlgorithm do the initialisation to the default of 
    the locals vars
    @ivar mutation: a mutation class (child of Mut)
    @type mutation: Mut
    @ivar crossover: a crossover class (child of CX)
    @type crossover: CX
    @ivar reduction: a reduction class (child of Reduction)
    @type reduction: Reduction
    @ivar evaluation: a evaluation class (child of GenericEvaluation)
    @type evaluation: GenericEvaluation
    @ivar nb_process: the number of process to use
    @type nb_process: int
    @ivar nb_generation: the number of generation to do
    @type nb_generation: int
    @ivar population_size: the population size
    @type population_size: int
    @ivar mutation_probability: the probability of mutation between 0 and 1
    @type mutation_probability: float
    @ivar crossover_probability: the probability of crossover between 0 and 1
    @type crossover_probability: float
    @ivar do_stats: if we will do the stat or not
    @type do_stats: bool
     """
    
    def __init__(self):
        self.mutation = None
        self.crossover = None
        self.reduction = None
        self.evaluation = None
        self.nb_process = 1
        self.nb_generation = 20
        self.population_size = 100 
        self.mutation_probability = 0.1
        self.crossover_probability = 0.4
        self.do_stats = False
    

class GeneticAlgorithm(GenericGeneticAlgorithm):
    """ GeneticAlgorithm contain the methods to run a standard
    Genetic Algorithm
    @ivar parents: a list of parents (child of Individual)
    @type parents: list(Individual)
    @ivar childs: a list of childs (child of Individual)
    @type childs: list(Individual)
    """
    
    def __init__(self, *args, **kwargs):
        """store all the given named parameted in the class"""
        super(GeneticAlgorithm ,self).__init__()
        for key, value in kwargs.items():
            setattr(self, key, value)
        self.parents = []
        #changed, now the population must have run before
        self.childs = self.population

    
    def do_crossover(self):
        """Do the crossover of the crossover class 
        given at the initialisation 
        (depend of the crossover_probability)"""  
        for i in range(0, len(self.parents)):
            if random.random() < self.crossover_probability:
                individual1 = random.choice(self.parents)
                individual2 = random.choice(self.parents)
                ind1, ind2 = self.crossover.run(individual1, individual2)
                self.childs.append(ind1)
                self.childs.append(ind2)
    
    def do_mutation(self):
        """ Do the mutation on the children depending of the 
        mutation probability
        @return: the number of mutation done"""
        nb_mutation = 0
        for individual in self.childs:
            if random.random() < self.mutation_probability:
                ind, muted = self.mutation.run(individual)
                if muted:
                    nb_mutation += 1
                    self.childs.append(ind)
        return nb_mutation
    
    def evaluate(self, individual):
        """do the evalutation of the individual
        @param individual: an individual
        @type individual: Individual
        @return: the individual and if it has been evaluated
        @rtype: (Individual, bool)"""
        ind, evaluated = self.evaluation.run(individual)
        return (ind, evaluated)
    
    def queueop(self, value, valres):
        """ usefull only in the case where we use multiprocess
        @param value: a result of evaluate 
        @type value: (Individual, bool)
        @param valres: a couple for the result a list of individual 
        and the number of modification
        @type valres: (list(Individual), int)
        @return: valres
        @rtype: (list(Individual), int)"""
        ind, evaluated = value
        if evaluated:
            valres[0].append(ind)
            valres[1] += 1
        return valres
        
    def do_evaluation(self):
        """ Do the evaluation of the whole child population
        and give the number of childs evaluated.
        Use the evaluation fuction given at the initialisation
        @return: the number of child evaluated
        @rtype: int"""
        #~ multiprocess 
        if self.nb_process > 1:
            pp = MultiProcessPool(self.evaluate, self.childs, self.queueop, [[],0], celery=self.celery)
            res = pp.run(self.nb_process)
            self.childs = res[0]
            return res[1]
        else:
            nb_evaluated = 0
            newpop = []
            for individual in self.childs:
                ind, evaluated = self.evaluation.run(individual)
                if evaluated:
                    newpop.append(ind)
                    nb_evaluated += 1
            self.childs = newpop
            return nb_evaluated
        
    def do_reduction(self):
        """ Do the reduction to get a population of the size given
        at the initialisation (Use the reduction function given at the 
        initialisation) """
        population = self.parents + self.childs
        self.parents = self.reduction.run(population, self.population_size)
        self.childs = []
        
    def get_worst_mean_best(self):
        """ get the worst score the best score and the mean score
        (used for stats)"""
        worst = self.parents[0].score
        best = self.parents[0].score
        mean = 0.
        for individual in self.parents:  
            if worst > individual.score:
                worst = individual.score
            if best < individual.score:
                best = individual.score
            mean += individual.score
        if self.reduction.goal == "minimize":
            temp = worst
            worst = best
            best = temp
        return worst, mean/len(self.parents), best
            
 
    def run(self):
        """ run the whole algorithm"""
        self.do_evaluation()
        self.parents = self.childs
        self.childs = []
        if self.do_stats:
            print "generation | worst | best | mean | nb_crossover | nb_mutation | nb_evaluation"
        for i in range(0, self.nb_generation):
            self.do_crossover()
            nb_crossover = len(self.childs)
            self.do_mutation()
            nb_mutation = len(self.childs) - nb_crossover
            nb_evaluation = self.do_evaluation()
            self.do_reduction()
            if self.do_stats:
                worst,  mean, best = self.get_worst_mean_best() 
                print str(i+1)+" | "+str(worst)+" | "+str(best)+" | "+str(mean)+" | "+str(nb_crossover)+" | "+str(nb_mutation)+" | "+str(nb_evaluation)
            
    def get_best_individual(self):
        """ Get the best individual
        @return: the best individual
        @rtype: Individual"""
        if self.reduction.goal == "minimize":
            return min(self.parents, key=lambda individual: individual.score)
        else:
            return max(self.parents, key=lambda individual: individual.score)
        
        
    
    
class GenericIslet(GeneticAlgorithm):
    """ GenericIslet represente an islet. It's derivated 
    from Genetic Algorithm. Our Islet can do only Differential Evolution
    so it doesn't need a crossover function
    @ivar send_pb: the probability if sending the information to another
    islet (between 0 and 1)
    @type send_pb: float
    @ivar min_pop: The minimal population per islet
    @type min_pop: int
    @ivar max_pop: the maximal population per islet
    @type max_pop: int
    @ivar nb_islet_created: The number of islet created
    @type nb_islet_created: int
    @ivar starting_generation: The generation to start with
    @type starting_generation: int
    @ivar islets: a list of islet (child of GenericIslet)
    @type islets: list(GenericIslet)
    @ivar communication: The communication method (child of GenericSending)
    @type communication: GenericSending
    """
    
    def __init__(self, *args, **kwargs):
        """ Initialisation of the Islet.
        Use kwargs to store all named param in the class"""
        self.send_pb = 1.
        self.min_pop = 100 #limit to not go bellow
        self.max_pop = 1000 #limit to not go above
        self.nb_islet_created = 0
        self.starting_generation = 0
        super(GenericIslet ,self).__init__(*args, **kwargs)
        #suppose to have something to send the state => in datasending
        #suppose to have something to compute the spacesearch 
        self.islets = []
        self.communication = eval(kwargs["communication"])
        
    def add_islet(self, islet):
        """ adding an islet to the list of islet we 
        can communicate with
        @param islet: a GenericIslet child to add
        @param islet: GenericIslet """
        self.islets.append(islet)
    
    def get_state(self):
        """ return the state of the population 
        @return: the state
        @rtype: dict(str:float)
        """
        state = {}
        #~ state["communication"] = self.communication
        state["population_size"] = self.population_size
        state["sum_vector"] = None
        state["mean_vector"] = None
        state["norm_mean_vector"] = None
        state["mean_norm_vector"] = None
        try:
            vectors = map(sum, zip(*[ind.vector for ind in self.parents]))
            state["sum_vector"] = vectors
            state["mean_vector"] = [vectorsum/self.population_size for vectorsum in vectors]
            state["norm_mean_vector"] = 0.
            try:
                for x in state["mean_vector"]:
                    state["norm_mean_vector"] += x**len(state["mean_vector"])
            except OverflowError:
                pass
            state["norm_mean_vector"] = abs(float(state["norm_mean_vector"]))**abs(1./float(len(state["mean_vector"])))
            norm_vectors = 0.
            for vector in [ind.vector for ind in self.parents]:
                norm = 0.
                for x in vector:
                    norm += x**len(vector)
                norm = abs(norm)**abs((1./float(len(vector))))
                norm_vectors += norm
            mean_norm_vector = norm_vectors/self.population_size
            state["mean_norm_vector"] = mean_norm_vector
        except (TypeError, ZeroDivisionError) as e:
            pass
        dnas = map(sum, zip(*[ind.dna for ind in self.parents]))
        state["gravity_center"] = [float(dnasum)/float(self.population_size) for dnasum in dnas]
        return state
    
    def send_state(self):
        """ Send the islet state to a randomly chose islet """
        if random.random() <= self.send_pb: 
            state = self.get_state()
            try:
                islet = random.choice(self.islets)
                self.communication.send_to(state, islet.communication)
            except IndexError:
                pass

    def change_popsize(self):
        """Compute a new population size with the state of other islet"""
        isdata = True
        while isdata:
            ourstate = self.get_state()
            isdata, data = self.communication.get_data()
            if data != None:
                old_pop = self.population_size
                try:
                    #formula need to be checked
                    self.population_size = self.population_size*(float(data["population_size"])/float(ourstate["population_size"]))*\
                                           (float(ourstate["norm_mean_vector"])/float(data["norm_mean_vector"]))
                    if self.population_size < 1. or self.population_size > self.max_pop*10 :
                        self.population_size = old_pop
                    self.population_size = max(self.min_pop, int(self.population_size))
                    self.population_size = min(self.max_pop, int(self.population_size))
                except ZeroDivisionError:
                    self.population_size = old_pop
    
    def try_divide_islet(self):
        """ divide the islet in some special cases"""
        ourstate = self.get_state()
        if ourstate["mean_norm_vector"] > ourstate["norm_mean_vector"] and self.population_size > 3*(self.max_pop/4):
            #do the quickseparation per spacesearch
            parents1 = []
            parents2 = []
            #avoid a pop of 0
            locked = 0
            while len(parents1) < self.population_size/5 or len(parents2) < self.population_size/5 or len(parents2)==0 or len(parents1)==0:
                qs = QuickSeparationAttr(self.parents, "dna")
                res = qs.separe_distance(2)
                parents1 = res[0]
                parents2 = res[1]
                locked += 1
                if locked == 5:
                    return
            self.population_size = self.population_size/2
            self.parents = parents1
            d_result = {"childs" : parents2, 
                        "population_size" : self.population_size,
                        "starting_generation" : self.starting_generation,
                        "from_islet" : self.islet_id}
            self.communication.send_to(d_result, self.islet_manager)

        
    def do_differential_evolution(self):
        """ do the differential evolution """
        bests = self.tournament_de.run(self.parents, self.nbselected_de)
        goal = self.tournament_de.goal
        #get and use the goal
        if goal == "maximize":
            rev = True
        else:
            rev = False
        bests = sorted(bests, key=lambda best: best.score, reverse=rev)[0: self.nbselected_de]
        try:
            maxi, mini = bests[0], bests[-1]
        except IndexError: #this error should not appear anymore
            print "ERROR TO CORRECT: bests :"
            print bests
            print "Parents len:"
            print len(self.parents)
        #vector construction
        vector = [ maxi.dna[i] - mini.dna[i] for i in range(0,len(maxi.dna)) ]
        for p in self.parents:
            #child contruction with constraints
            self.childs.append(self.differential_evolution_constraintes.child_creation(p, vector))
    
    def run(self):
        """ define the process depending of the different parameters"""
        self.do_evaluation()
        self.parents = self.childs
        #calcul des vecteurs
        self.childs = []
        start = self.starting_generation
        for i in range(start, self.nb_generation):
            self.starting_generation += 1
            self.do_differential_evolution()
            nb_mutation = self.do_mutation()
            nb_evaluation = self.do_evaluation()
            self.change_popsize()
            self.do_reduction()
            self.send_state()
            self.try_divide_islet()
            if self.do_stats:
                worst, mean, best = self.get_worst_mean_best() 
                print str(i+1)+" | "+str(self.islet_id)+" | "+str(worst)+" | "+str(best)+" | "+str(mean)+" | "+str(nb_mutation)+" | "+str(nb_evaluation)+" | "+str(self.population_size)
        #print of the best individual
        
        
class IsletManager(object):
    """ Manage multiple islet
    @ivar islets: the list of all islets (child of GenericIslet)
    @type islets: list(GenericIslet)
    @ivar kwargs: the kwargs in entry
    @type kwargs: dict(str:any)
    @ivar args: the args in entry
    @type args: list(any)
    @ivar communication: The communication method (child of GenericSending)
    @type communication: GenericSending
    @ivar do_stats: if we do the stats
    @type do_stats: bool
    """

    def __init__(self, *args, **kwargs):
        """ Create the firsts islets, their population, divide the population and
        inform each islet of the existance of the other islets"""
        self.islets = []
        populations = kwargs["population"].run(kwargs["population_size"]*kwargs["nb_islet"], kwargs["nb_islet"])
        self.kwargs = kwargs
        self.args = args
        self.communication = eval(kwargs["communication"])
        self.do_stats = kwargs.get("do_stats", False)
        for i in range(0, kwargs["nb_islet"]):
            kwargs["population"] = populations[i]
            istmp = GenericIslet(islet_manager=self.communication, islet_id=str(i), *args, **kwargs)
            self.islets.append(istmp)
        #~ #adding islets in islets
        for islet1 in self.islets:
            for islet2 in self.islets:
                if islet2 != islet1:
                    islet1.add_islet(islet2)
   
    def create_new_islet(self, childs, population_size, starting_generation, from_islet):
        """ method to create a new islet when there is a division
        @param childs: a list of individual (Individual child) to give to the new islet
        @type childs: list(Individual)
        @param population_size: the population size to reach after each reduction
        @type population_size: int
        @param starting_generation: the current generation for the new islet
        @type starting_generation: int
        @param from_islet: the islet (child of Genericislet) that divide
        @type from_islet: GenericIslet
        """
        #~ if len(childs) == 0:
        gi = GenericIslet(islet_manager=self.communication, islet_id=str(len(self.islets)), *self.args, **self.kwargs)
        #to not fail on the evaluation
        for child in childs:
            child.score = None 
        gi.childs = childs
        gi.population_size = population_size
        gi.starting_generation = starting_generation
        for islet in self.islets:
            if islet.islet_id == from_islet:
                gi.add_islet(islet)
        for islet in self.islets:
            islet.add_islet(gi)
        self.islets.append(gi)
        pgi = Process(target=gi.run, args=())
        pgi.start()
        self.procs.append(pgi)

    def data_receiver(self):
        """ Check if receive any divide order for the whole running process"""
        working = True
        while working:
            isdata = True
            while isdata:
                isdata, data = self.communication.get_data()
                if data != None:
                    self.create_new_islet(childs=data["childs"],
                                          population_size=data["population_size"],
                                          starting_generation=data["starting_generation"],
                                          from_islet=data["from_islet"])
            working = False
            for p in self.procs:
                if p.is_alive():
                    working = True
    
    def start_islets(self):
        """ start the islets """
        if self.do_stats:
            print "generation | islet | worst | best | mean | nb_mutation | nb_evaluation | pop_size"
        self.procs = [Process(target=islet.run, args=()) for islet in self.islets]
        for p in self.procs:
            p.start()

    def run(self):
        """ run the islet manager"""
        self.start_islets()
        self.data_receiver()
        for p in self.procs:
            p.join()
        
    
    
    
    

