#!/usr/bin/env python
# encoding: utf-8
import random

from individual import Individual


class Reduction(object):
    """ Generic Reduction Method to be inherited"""
    
    def __init__(self, **kwargs):
        """ store every kwargs in the class"""
        for key, value in kwargs.items():
            setattr(self, key, value)
    
    def reduce(self, population, number):
        """to implement in other reduction class : a reduction function
        @param population: the population to reduce
        @type population: list(Individual)
        @param number: the size of the population to reach
        @type number: int
        @return: the selected population
        @rtype: list(Individual)
        """
        raise NotImplementedError
    
    def run(self, population, number):
        """ do the reduction process only if the population is
        bigger than the number we want to reach
        @param population: the population to reduce
        @type population: list(Individual)
        @param number: the size of the population to reach
        @type number: int
        @return: the selected population
        @rtype: list(Individual)
        """ 
        #if we try to select more than the population
        if len(population) <= number:
            return population
        return self.reduce(population, number)


class SelTournament(Reduction):
    """ Selection tournament. Can maximize of minimize
    @ivar tournsize: the number of individual involved in each tournament
    @param tournsize: int
    """
    #self.goal (maximaze, minimaze), number (number of individual to select), self.tournsize
    
    def reduce(self, population, number):
        """ return the reduce population to number
        @param population: the population to reduce
        @type population: list(Individual)
        @param number: the size of the population to reach
        @type number: int
        @return: the selected population
        @rtype: list(Individual)"""
        selected = []
        while len(selected) < number:
            tournament = []
            #select the elems
            while len(tournament) < self.tournsize and len(population) > 0:
                tournament.append(random.choice(population))
            #do the tournament
            curscore = None
            curelem = None
            if self.goal == "maximize":
                for elem in tournament:
                    if curscore == None or curscore < elem.score:
                        curscore = elem.score
                        curelem = elem
            elif self.goal == "minimize":
                for elem in tournament:
                    if curscore == None or curscore > elem.score:
                        curscore = elem.score
                        curelem = elem
            selected.append(curelem)
        return selected
        
class SelTournamentNoRemise(Reduction):
    """ Selection tournament without remise of the selected individual
    @ivar tournsize: the number of individual involved in each tournament
    @param tournsize: int"""
    
    def reduce(self, population, number):
        """ return the reduce population to number
        @param population: the population to reduce
        @type population: list(Individual)
        @param number: the size of the population to reach
        @type number: int
        @return: the selected population
        @rtype: list(Individual)"""
        selected = []
        while len(selected) < number:
            tournament = []
            #select the elems
            while len(tournament) < self.tournsize and len(population) > 0:
                i = random.randint(0, len(population)-1)
                tournament.append(population.pop(i))
            #do the tournament
            curscore = None
            curelem = None
            otherelem = []
            if self.goal == "maximize":
                for elem in tournament:
                    if curscore == None or curscore < elem.score:
                        if curelem != None:
                            otherelem.append(curelem)
                        curscore = elem.score
                        curelem = elem
                    else:
                        otherelem.append(elem)
            elif self.goal == "minimize":
                for elem in tournament:
                    if curscore == None or curscore > elem.score:
                        if curelem != None:
                            otherelem.append(curelem)
                        curscore = elem.score
                        curelem = elem
                    else:
                        otherelem.append(elem)
            selected.append(curelem)
            for elem in otherelem:
                population.append(elem)
        return selected

if __name__ == "__main__":
    pop = []
    for i in range(0, 42):
        pop.append(Individual([32, 21]))
        pop[i].score = i
    seltourn = SelTournamentNoRemise(goal="maximaze", tournsize=4)
    res = seltourn.run(pop, number=7)
    
    for elem in res:
        print elem.score
    #~ print i2.dna, i1.dna 

