import random, math

from algorithm import IsletManager
from crossover import CXTwoPoints
from mutation import MutUniform
from reduction import SelTournamentNoRemise, SelTournament
from evaluation import GenericEvaluation
from individual import PopulationQuickSeparation
from spacesearch import DEConstraints

#Rosenbrock is unimodal

class PopEggholder(PopulationQuickSeparation):
    
    def dna_generation(self):
        dna = []
        for i in range(0, self.list_len):
            dna.append(random.uniform(-400, 400))
        return dna

    def vector_computation(self, dna):
        vector = [0]*self.list_len
        return vector
    

class EvalEggholder(GenericEvaluation):
    
    def evaluate(self, individual_dna):
        """ do the score on weierstrass function""" 
        score = 0.
        for i in range(0,len(individual_dna)/2-1):
            score += self.value_function(individual_dna[i], individual_dna[i+len(individual_dna)/2])
        score = 0.5*score
        return score

    def value_function(self, x, y):
        return -(y+47.)*math.sin(math.sqrt(abs(x/2.+y+47.)))-x*math.sin(math.sqrt(abs(x-y+47.)))
        
        
if __name__ == "__main__":
    
    population = PopEggholder(list_len=20) # 10*10
    evaluation = EvalEggholder()
    reduction = SelTournament(goal="maximize", tournsize=7)
    tournament_de = SelTournament(goal="maximize", tournsize=7)
    deconstraints = DEConstraints([-400.]*20, [400.]*20)
    mutation = MutUniform(indpb=0.1, low=-400, up=400)
    
    algo = IsletManager(mutation=mutation, reduction=reduction, 
                        evaluation=evaluation, population=population,
                        population_size=400, nb_generation=20, 
                        mutation_probability=0.1, 
                        do_stats=True, nb_islet=4, communication="QueueSending()",
                        tournament_de=tournament_de, nbselected_de=40,
                        differential_evolution_constraintes=deconstraints,
                        max_pop=1000, min_pop=100)
    algo.run()
