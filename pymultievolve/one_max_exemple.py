#!/usr/bin/env python
# encoding: utf-8
import random


from algorithm import GeneticAlgorithm, GenericIslet
from crossover import CXTwoPoints
from mutation import MutUniformInt
from reduction import SelTournamentNoRemise
from evaluation import GenericEvaluation
from individual import Population

class PopOneMax(Population):
    
    def dna_generation(self):
        dna = []
        for i in range(0, self.list_len):
            dna.append(random.randint(0,1))
        return dna
        
class EvalOneMax(GenericEvaluation):
    
    def evaluate(self, individual_dna):
        score = 0
        for number in individual_dna:
            score += number
        return score


if __name__ == "__main__":
    population = PopOneMax(list_len=50)
    poptosend = population.run(1000)
    evaluation = EvalOneMax()
    #~ reduction = SelTournamentNoRemise(goal="minimize", tournsize=7)
    reduction = SelTournamentNoRemise(goal="maximize", tournsize=7)
    crossover = CXTwoPoints()
    mutation = MutUniformInt(indpb=0.1, low=0, up=1)
    
    algo = GeneticAlgorithm(mutation=mutation, crossover=crossover, 
                            reduction=reduction, evaluation=evaluation, 
                            population=poptosend,
                            population_size=1000, nb_generation=20, 
                            mutation_probability=0.1, crossover_probability=0.4, 
                            do_stats=True)
    algo.run()
    print algo.get_best_individual()
    
    
    
