import random, math


from algorithm import IsletManager
from crossover import CXTwoPoints
from mutation import MutUniform
from reduction import SelTournamentNoRemise, SelTournament
from evaluation import GenericEvaluation
from individual import PopulationQuickSeparation
from spacesearch import DEConstraints

#Rosenbrock is unimodal

class PopWeierStrass(PopulationQuickSeparation):
    
    def dna_generation(self):
        dna = []
        for i in range(0, self.list_len):
            dna.append(random.uniform(-2.5
            , 2.5))
        return dna

    def vector_computation(self, dna):
        vector = [0]*self.list_len
        return vector
    

class EvalWeierStrass(GenericEvaluation):
    
    def evaluate(self, individual_dna):
        """ do the score on weierstrass function""" 
        score = 0.
        for ind_val in individual_dna:
            score += self.value_function(ind_val)
        return score

    def value_function(self, x):
        b = 20.
        a = 0.5
        n = 5
        val = 0.
        for i in range(0, n):
            val += (a**i)*math.cos(b**i*math.pi*x)
        return val
        
        

if __name__ == "__main__":
    
    population = PopWeierStrass(list_len=10) # 10*10
    evaluation = EvalWeierStrass(min_x=-2.5, max_x=2.5)
    reduction = SelTournament(goal="maximize", tournsize=7)
    tournament_de = SelTournament(goal="maximize", tournsize=7)
    deconstraints = DEConstraints([-2.5]*10, [2.5]*10)
    mutation = MutUniform(indpb=0.1, low=-2.5, up=2.5)
    
    algo = IsletManager(mutation=mutation, reduction=reduction, 
                        evaluation=evaluation, population=population,
                        population_size=400, nb_generation=20, 
                        mutation_probability=0.1, 
                        do_stats=True, nb_islet=4, communication="QueueSending()",
                        tournament_de=tournament_de, nbselected_de=40,
                        differential_evolution_constraintes=deconstraints,
                        max_pop=1000, min_pop=100)
    algo.run()
