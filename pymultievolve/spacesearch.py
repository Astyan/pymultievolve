#!/usr/bin/env python
# encoding: utf-8


#all ways to compute the space search 
# in the projet it suppose to be the sum of the norm of the vector
from copy import deepcopy
import random


class DEConstraints(object):
    """Constraintes in differential evolution 
    to not go over the spacesearch 
    @ivar mindna: a dna exemple of with every minimal element
    @type mindna: list(int) or list(float)
    @ivar maxdna: a dna exemple of with every maximal element
    @type maxdna: list(int) or list(float)"""
    
    def __init__(self, mindna, maxdna):
        """init with a maximal dna and a minimal dna as limit 
        @param mindna: a dna exemple of with every minimal element
        @type mindna: list(int) or list(float)
        @param maxdna: a dna exemple of with every maximal element
        @type maxdna: list(int) or list(float)
        """
        self.mindna = mindna
        self.maxdna = maxdna
    
    def child_creation(self, child, vector):
        """Create a child respecting constrains from a parent 
        and a vector
        @param child: the child to pass transmorph in a valid child if
        it's not
        @type child: Individual
        @param vector: the vector generated while the creation
        @type vector: list(float) or list(int)
        @return: the valid child
        @rtype: Individual"""
        child = deepcopy(child)
        new_dna = []
        for i in range(0, len(vector)):
            if child.dna[i] + vector[i] < self.mindna[i]:
                new_dna.append(self.mindna[i])
            elif child.dna[i] + vector[i] > self.maxdna[i]:
                new_dna.append(self.maxdna[i])
            else:
                new_dna.append(child.dna[i] + vector[i])
        child.vector = vector
        child.dna = new_dna
        child.score = None
        return child
    
    
class QuickSeparation(object):
    """ QuickSeparation process. Close to a kmeans without
    any iteration to save time.
    Can use only dna like list
    @ivar population: the population
    @type population: list(Individual)
    @ivar separatedpop: the separated populaton
    @type separatedpop: list(list(Individual))
    @ivar selected: the selected center
    @type selected: list(Individual)"""
    
    def __init__(self, pop):
        """ simple init
        @param pop: the population
        @type pop: list(Individual)
        """ 
        self.population = pop
        self.separatedpop = []
        self.selected = []
        
    def select(self, nb_separation):
        """ Select the centers
        @param nb_separation: the number of separation or center wanted
        @type nb_separation: int"""
        for i in range(0, nb_separation):
            self.selected.append(random.choice(self.population))
    
    def init_separatedpop(self, nb_separation):
        """ init all the population with empty list 
        @param nb_separation: the number of separation or center wanted
        @type nb_separation: int"""
        for i in range(0, nb_separation):
            self.separatedpop.append([])
    
    def distance(self, v1, v2):
        """ compute a euclidian distance between two list (point in any dimention)  
        @param v1: the first point
        @type v1: list(float) or list(int)
        @param v2: the second point
        @type v2: list(float) or list(int)
        @return: the distance
        @rtype: float"""
        if len(v1) != len(v2):
            raise NotImplementedError
        cumulated_sum = 0
        for i in range(0, len(v1)):
            cumulated_sum += (v1[i]-v2[i]) ** 2
        res = float(cumulated_sum) ** (0.5)
        return res
    
    def closer(self, vector):
        """ return the list number where the vector must be save
        it's the list where the vector is closer from the center
        @param vector: a list of int or float
        @type vector: list(int) or list(float)
        @return: the list number where to put this vector
        @rtype: int
        """
        distances = []
        for s_vector in self.selected:
            distances.append(self.distance(vector, s_vector))
        min_distance = min(distances)
        for i in range(0, len(distances)):
            if distances[i] == min_distance:
                return i
            
    def separe_distance(self, nb_separation):
        """Separe by the distance
        @param nb_separation: the number of separation to do
        @type nb_separation: int
        @return: the separated population
        @rtype: list(list(Individual)))"""
        self.select(nb_separation)
        self.init_separatedpop(nb_separation)
        for vector in self.population:
            pop_to_go = self.closer(vector)
            self.separatedpop[pop_to_go].append(vector)
        return self.separatedpop

class QuickSeparationAttr(QuickSeparation):
    """ Same a quick separation but use an attribute (
    Because Individual have dna)
    @ivar population: the population
    @type population: list(Individual)
    @ivar separatedpop: the separated populaton
    @type separatedpop: list(list(Individual))
    @ivar selected: the selected center
    @type selected: list(Individual)
    @ivar attr: a given attribute (dna, vector, etc.) to do the 
    separation with
    @type attr: str
    """
    
    def __init__(self, pop, attr):
        super(QuickSeparationAttr, self).__init__(pop)
        self.attr = attr
    
    def distance(self, v1, v2):
        """ distance compute with the attr
        @param v1: the first point
        @type v1: list(float) or list(int)
        @param v2: the second point
        @type v2: list(float) or list(int)
        @return: the distance
        @rtype: float"""
        if len(eval("v1."+self.attr)) != len(eval("v2."+self.attr)):
            raise NotImplementedError
        cumulated_sum = 0
        for i in range(0, len(eval("v1."+self.attr))):
            cumulated_sum += (eval("v1."+self.attr)[i]-eval("v2."+self.attr)[i]) ** 2
        res = float(cumulated_sum) ** (0.5)
        return res
    
    
if __name__ == "__main__":
    pop = [[1,2],[3,4],[2,2],[2,1],[5,2],[1,3],[3,2],[4,2],[6,6],[6,2]]
    qs = QuickSeparation(pop)
    print qs.separe_distance(3)
    
        
        
        
        
        
        
        
        
        
