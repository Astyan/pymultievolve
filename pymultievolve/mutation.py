#!/usr/bin/env python
# encoding: utf-8
import random
import copy

from individual import Individual

class Mut(object):
    """ simple mutation class. It's a parent class, any mutation must
    be child of this class"""
    
    def __init__(self, **kwargs):
        """ simple init that store all kwargs"""
        for key, value in kwargs.items():
            setattr(self, key, value)
    
    def mute(self, individual):
        """ mute an individual
        @param individual: an individual to mute 
        @type individual: Individual
        @return: the muted individual and if it's have been muted
        @rtype: (Individual, bool)
        """
        raise NotImplementedError

    def run(self, individual):
        """ 
        do the mutation and change the score
        @param individual: an individual to mute 
        @type individual: Individual
        @return: the muted individual and if it's have been muted
        @rtype: (Individual, bool)
        """
        ind, muted = self.mute(copy.deepcopy(individual))
        if muted:
            ind.score = None
        return ind, muted
        
class MutUniformInt(Mut):
    """Uniform mutation with int. Usable if the dna is a list 
    @ivar indpb: the probability of mutationf for each element in 
    the dna (0 to 1)
    @type indpb: float
    @ivar low: the lower value a dna element can take
    @type low: int
    @ivar up: the maximal value a dna element can take
    @type up: int"""
    
    def mute(self, individual):
        """ uniform mutation of an individual
        @param individual: an individual to mute 
        @type individual: Individual
        @return: the muted individual and if it's have been muted
        @rtype: (Individual, bool)
        """
        muted = False
        dna = individual.dna
        new_dna = []
        for val in dna:
            if random.random() <= self.indpb:
                muted = True
                new_dna.append(random.randint(self.low, self.up))
            else:
                new_dna.append(val)
        individual.dna = new_dna
        return individual, muted

class MutUniform(Mut):
    """Uniform mutation with float. Usable if the dna is a list 
    @ivar indpb: the probability of mutationf for each element in 
    the dna (0 to 1)
    @type indpb: float
    @ivar low: the lower value a dna element can take
    @type low: float
    @ivar up: the maximal value a dna element can take
    @type up: float"""

    
    def mute(self, individual):
            """ uniform mutation of an individual
        @param individual: an individual to mute 
        @type individual: Individual
        @return: the muted individual and if it's have been muted
        @rtype: (Individual, bool)
        """
        muted = False
        dna = individual.dna
        new_dna = []
        for val in dna:
            if random.random() <= self.indpb:
                muted = True
                new_dna.append(random.uniform(self.low, self.up))
            else:
                new_dna.append(val)
        individual.dna = new_dna
        return individual, muted

if __name__ == "__main__":
    i1 = Individual([32, 21])
    mutuniform = MutUniformInt(indpb=0.5, low=10, up=42)
    ind, muted = mutuniform.run(i1)
    print ind.dna
