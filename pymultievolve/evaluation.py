#!/usr/bin/env python
# encoding: utf-8
import random

from individual import Individual

class GenericEvaluation(object):
    """ Genety evaluation class. Any Evaluation class must be child of
    this class"""
    
    def __init__(self, **kwargs):
        """ simple init saving the kwargs """
        for key, value in kwargs.items():
            setattr(self, key, value)
    
    def evaluate(self, individual_dna):
        """ evaluate the dna of an individual
        @param individual_dna: the dna of an individual
        @type individual_dna: any
        @return: the score
        @rtype: float
         """
        raise NotImplementedError
    
    def run(self, individual):
        """ run the evaluation and save the score of evaluation in the
        individual
        @param individual: an individual 
        @type individual: Individual
        @return: the individual and if the individual have been evaluated
        @rtype: (Individual, bool)
        """
        
        #do the evaluation only if the individual need a new evaluation
        evaluated = False
        #~ print individual.score, individual.dna 
        if individual.score == None:
            evaluated = True
            individual.score = self.evaluate(individual.dna)
        return individual, evaluated
        
