#!/usr/bin/env python
# encoding: utf-8

#~ from cluster import KMeansClustering
from spacesearch import QuickSeparation

class Individual(object):
    """ A simple individual object
    @ivar score: the score of the individual
    @type score: float
    @ivar vector: the vector used to create the individual
    @type vector: list(any)
    @ivar dna: the dna of the individual
    @type dna: any
    """
    
    def __init__(self, dna, vector):
        """ simple init storing the dna and the vector
        @param dna: the dna
        @type dna: any
        @param vector: a vector 
        @type vector: list
        """
        self.score = None
        self.vector = vector
        self.dna = dna

class Population(object):
    """ An object to inherit from.
    It's allow to generate a population with dna and vector if needed"""
    
    def __init__(self, **kwargs):
        """ store every kwargs in the class"""
        for key, value in kwargs.items():
            setattr(self, key, value)
    
    def dna_generation(self):
        """ How to generate DNA. Method to change per the user"""
        raise NotImplementedError
    
    def vector_computation(self, dna):
        """ How to generate the first vector.
        Method to change per the user if vector is needed
        @param dna: the dna of the individual
        @type dna: any
        @return: the generated vector
        @rtype: list"""
        return None
        
    def run(self, size):
        """ The method to create a population of a given size 
        @param size: the size of the population wanted
        @type size: int
        @return: the population created
        @rtype: list(Individual)"""
        pop = []
        for i in range(0, size):
            dna = self.dna_generation()
            vector = self.vector_computation(dna)
            pop.append(Individual(dna, vector))
        return pop
        

class PopulationQuickSeparation(Population):
    """ Change the running method to allow a
    separation per space search 
    Usefull when using multiple Islet"""

    def run(self, size, nb_cluster):
        """ The method to create a population of a given size divided
        between multiple cluster
        @param size: the total number of the population
        @type size: int
        @param nb_cluster: the number of cluster of pop we want
        @type nb_cluster: int
        @return: the clusterised population
        @rtype: list(list(Individual))
        """
        pop = []
        dnapop = []
        for i in range(0, size):
            dna = self.dna_generation()
            dnapop.append(tuple(dna))
        qs = QuickSeparation(dnapop)
        clusters = qs.separe_distance(nb_cluster)
        for cluster in clusters:
            popcluster = []
            for dna in cluster:
                vector = self.vector_computation(dna)
                popcluster.append(Individual(dna, vector))
            pop.append(popcluster)
        return pop


